sealed trait Device

case class Phone(model: String) extends Device {
  def screenOff = "Turning screen off" + model
}

case class Computer(model: String) extends Device {
  def screenSaverOn = "Turning screen saver on..." + model
}

def goIdle(device: Device) = device match {
  case p: Phone => p.screenOff match {
    case s: String if s.contains("iPhone") => "Apple"
    case _ => "Other"
  }
  case c: Computer => c.screenSaverOn
}