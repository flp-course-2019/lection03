sealed trait Notification

case class Email(sender: String, title: String, body: String) extends Notification

case class SMS(caller: String, message: String) extends Notification

case class VoiceRecording(contactName: String, link: String) extends Notification



def showImportantNotification(notification: Notification, importantPeopleInfo: Seq[String]): String = {
  notification match {
    case Email(email, _, _) if importantPeopleInfo.contains(email) =>
      "You got an email from special someone!"
    case s@SMS(number, _) if importantPeopleInfo.contains(number) =>
      "You got an SMS from " + s.message
    case _ =>
      ???
  }
}

val importantPeopleInfo = Seq("867-5309", "jenny@gmail.com")

val someSms = SMS("867-5309", "Are you there?")
val someVoiceRecording = VoiceRecording("Tom", "voicerecording.org/id/123")
val importantEmail = Email("jenny@gmail.com", "Drinks tonight?", "I'm free after 5!")
val importantSms = SMS("867-5309", "I'm here! Where are you?")

showImportantNotification(someSms, importantPeopleInfo)
showImportantNotification(someVoiceRecording, importantPeopleInfo)
showImportantNotification(importantEmail, importantPeopleInfo)
showImportantNotification(importantSms, importantPeopleInfo)