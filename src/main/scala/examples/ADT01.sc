sealed trait Exp

case class Val(name: String) extends Exp
case class Fun(arg: String, body: Exp) extends Exp
case class App(f: Exp, v: Exp) extends Exp

val y = Fun("x", Fun("y", App(Val("x"), Val("y"))))
