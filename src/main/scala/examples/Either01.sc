sealed trait Either[+A, +B]

case class Left[A](a: A)  extends Either[A, Nothing]
case class Right[B](b: B) extends Either[Nothing, B]




def safeDiv(a: Int, b: Int): Either[String, Int] =
  if (b != 0) Right(a / b) else Left("Divide by zero!")

val x = safeDiv(10, 2)

val y = safeDiv(10, 0)