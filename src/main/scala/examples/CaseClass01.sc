case class Test(arg: String)

/*
object Test {
  def apply(arg: String) = ???
  def unapply(arg: String) = ???
  override def toString = ???
  override def hashCode() = ???
  def copy(arg: String) = ???
}*/

sealed trait Exp
case class Val(name: String) extends Exp
case class Fun(arg: String, body: Exp) extends Exp
case class App(f: Exp, v: Exp) extends Exp

val y = Fun("x", Fun("y", App(Val("x"), Val("y"))))