case class Person(first: String, last: String,
                  age: Int = 0, ssn: String = "")

val p1 = Person("Fred", "Jones", 23,
  "111-22-3333")


val parts = Person.unapply(p1).get


parts._1
parts._2


case class Persona(serviceName: String,
                   serviceId: String,
                   sentMessages: Set[String])

val existingPersona = Persona("Name","id000", Set("hello", "bot"))

val newMessage = "new message"

val newPersona: Persona = existingPersona
  .copy(sentMessages = existingPersona.sentMessages +
  newMessage)