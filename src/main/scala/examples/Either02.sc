val input:String = "str"

val result: Either[String,Int] = try {
  Right(input.toInt)
} catch {
  case e: Exception =>
    Left(input.toString)
}

result match {
  case Right(x) =>
    s"$x + 1 = ${x+1}"
  case Left(x)  =>
    s"You passed me the String: $x"
}