def echoWhatYouGaveMe(x: Any): String = x match {

  case (a, b) if a.toString.contains("1") => s"got $a as string and $b"
  case (a, b, c) => s"got $a, $b, and $c"

  case _ => "Unknown"
}

echoWhatYouGaveMe((1,2))