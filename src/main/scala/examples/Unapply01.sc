import scala.util.Random

object CustomerID {

  def apply(name: String) = s"$name--${Random.nextLong}"

  def unapply(customerID: String): Option[String] = {
    val stringArray: Array[String] = customerID.split("--")
    if (stringArray.tail.nonEmpty) Some(stringArray.head)
    else None
  }
}

val customer1ID = CustomerID("ID/87654321")


customer1ID match {
  case CustomerID(name) => name
  case _ => "Could not extract a CustomerID"
}


val customer2ID = CustomerID("Nico")
val CustomerID(name) = customer2ID

CustomerID.unapply(customer2ID).get

