case class Person(first: String, last: String, age: Int =
0, ssn: String = "")

val a1 = Person("Fred", "Jones", 23, "111-22-3333")
val a2 = Person("Anna", "Jones")
val a3 = Person(last = "Jones", first = "Fred", ssn =
  "111-22-3333")

a1 == a2
a1 == a3

a1 eq a2
a1 eq a3

a3.copy(age = 23)
a3.copy(ssn = "222-33-4444")

a1 == a3.copy(age = 23)