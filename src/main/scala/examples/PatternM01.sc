val times = 1

times match {
  case 1 => "one"
  case 2 => "two"
  case _ => "some other number"
}

times match {
  case i if i == 1 => "one"
  case i if i == 2 => "two"
  case _ => "some other number"
}


def matchTest(x: Int): String = x match {
  case 1 => "one"
  case 2 => "two"
  case 3 | 4 | 5  => "3 | 4 | 5"
  case _ => "many"
}

matchTest(10)
matchTest(1)